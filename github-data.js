/**
 * User: Jose V. Trigueros
 */

var zlib = require('zlib'),
    fs = require('fs'),
    http = require('http');

// Obtains a json archive file from the GitHub Archives
exports.downloadGithubArchive = function(baseUrl, fileRequest) {
    var request = http.get(baseUrl + fileRequest + '.gz');
    request.on('response', function(res) {
        var out = fs.createWriteStream(fileRequest);
        var gzip = res.pipe(zlib.createGunzip());

        res.on('end', function() {
            console.log("finished unzip");
            gzip.pipe(out);
            if( fs.existsSync(fileRequest) )
                console.log("exists")
        });

    });
}